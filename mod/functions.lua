--functions.lua

-- Faction Commands

-- -- Management
function createFaction(player, faction, password)
 	if ~game,forces[faction]:
 		return 
 	newfaction = game.create_force(faction)
 	player.force = newfaction
 end
function deleteFaction(player, faction, password) -- merges the faction with the default faction
 	game.merge_force(game.player.force, "player")
 end
function mergeFaction(player, faction, password)
 	game.merge_force(game.player.force, faction)
 end 

-- -- Membership Commands
function joinFaction(player, faction, password)
	player.force = faction
end
function leaveFaction(player)
	player.force = "player"
end


function factions() -- lists all the factions
	for force in game.forces{
		membercount = 0
		for member in force.players do{
			membercount = membercount + 1
		}
		forces = ("\nName: <factionnamehere> -- Member Count: {membercount}")
	}
end
function faction(faction) -- lists info about the specified faction
	force = faction
	for member in force.players do{
		membercount = membercount + 1
	}
	forces = ("\nName: <factionnamehere>\nMember Count: {membercount}")
end

-- -- Faction Relation Commands
function createAlliance(player. faction)
	game.player.force.set_cease_fire(faction, true)
	game.player.force.set_friend(faction, true)
end

function endAlliance(player. faction)
	game.player.force.set_cease_fire(faction, false)
	game.player.force.set_friend(faction, false)
end

function ceaseFire(player, faction, time)
	game.player.force.set_cease_fire(faction, true)
	ceaseFireTime(player.force, faction, time)
end
function ceaseFireTime(faction1, faction2, time) -- Internal --also not done
	-- Sets a timer
end

