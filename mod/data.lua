--data.lua
require("functions")

data:extend({
	local commands = {
		{
			name = "createfaction", 
			help = "Creates a joinable faction", 
			func = createFaction
		},
		{
			name = "deletefaction"
			help = "Deletes a faction and merges all assets to the Public Faction",
			func = deleteFaction
		}
		{
			name = "joinfaction",
			help = "Joins a faction",
			func = joinFaction
	    },
	    {
	    	name = "leavefaction",
			help = "Leaves your current faction and rejoins the default faction",
			func = leaveFaction
		}
	}
	for _, c in pairs(commands) d
		commands.add_command(c.name, c.help, c.func)
	end
})